import 'dart:io';

import 'package:count/src/models/UsuarioModel.dart';
export 'package:count/src/models/UsuarioModel.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DB {
  static Database _database;
  static final DB db = DB._();

  DB._();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
      print("ya existe base de datos");
    }
    _database = await initDB();
      return _database;
  }
  
   initDB() async {

     Directory documentsDirectory = await getApplicationDocumentsDirectory();
    
    final path = join( documentsDirectory.path, 'ScansDB.db');
    
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async{
        await db.execute(
          'CREATE TABLE usuario ('
          ' id INTEGER PRIMARY KEY,'
          ' nombre TEXT,'
          ' apellido TEXT,'
          ' direccion TEXT'
          ')'
        );

      }
    );
  }

  //CREATE
  nuevoUsuario(UsuarioModel nuevoUser) async{

    final db = await database;
    final res = await db.insert('usuario', nuevoUser.toJson());
    print("se creo un usuario");
    return res;

  }
  //LISTA DE USUARIOS
  Future<List<UsuarioModel>> getUsuarios() async{
    final db = await database;
    final res = await db.query('usuario');

    List<UsuarioModel> list = res.isNotEmpty 
      ? res.map( (e) => UsuarioModel.fromJson(e)).toList()
    :[];
    print("lista de elememtos");
    print(list.length);
    return list;
    //crear una lista MAP de usuarioModel
  }
  //GET
  Future<UsuarioModel> getUsuarioID(int id) async{
    final db = await database;
    final res = await db.query(
      'usuario',
      where: 'id = ?',
      whereArgs: [id]);

    return res.isNotEmpty ? UsuarioModel.fromJson(res.first): null ; 
  }
  //DELET

  deletUser(int id) async{

    final db = await database;
    final res = await db.delete('usuario',where: 'id = ?', whereArgs: [id]);
  }
}
