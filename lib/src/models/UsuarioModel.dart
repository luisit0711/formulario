class UsuarioModel{
    int id;
    String nombre;
    String apellido;
    String direccion;

  UsuarioModel({
    this.id,
    this.nombre,
    this.apellido,
    this.direccion
  });

  factory UsuarioModel.fromJson(Map<String,dynamic> json) => new UsuarioModel(
    id:json["id"],
    nombre: json["nombre"],
    apellido: json["apellido"],
    direccion: json["direccion"]
  );

  Map<String, dynamic> toJson()=> {
    "id": id,
    "nombre": nombre,
    "apellido": apellido,
    "direccion": direccion
  };
}