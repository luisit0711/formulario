import 'dart:async';

import 'package:count/src/db/db.dart';

class UsuarioBloc {
  static final UsuarioBloc _singleton = new UsuarioBloc._internal();

  factory UsuarioBloc() {
    return _singleton;
  }

  UsuarioBloc._internal() {
    obtenerUsuarios();
  }

  final _usuarioController = StreamController<List<UsuarioModel>>.broadcast();

  Stream<List<UsuarioModel>> get scansStream => _usuarioController.stream;
  //Stream<List<ScanModel>> get scansStreamHttp => _scansController.stream.transform(validarHttp);

  dispose() {
    _usuarioController?.close();
  }

  obtenerUsuarios() async {
    _usuarioController.sink.add(await DB.db.getUsuarios());
  }

  agregarUsuario(UsuarioModel user) async {
    await DB.db.nuevoUsuario(user);
    obtenerUsuarios();
  }

  borrarUsuario(int id) async {
    await DB.db.deletUser(id);
    obtenerUsuarios();
  }

  borrarScanTODOS() async {
    //await DBProvider.db.deleteAll();
    //obtenerScans();
  }
}
