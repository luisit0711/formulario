import 'package:count/src/bloc/usuario_bloc.dart';
import 'package:count/src/db/db.dart';
import 'package:flutter/material.dart';

class ListUsuariosPage extends StatelessWidget {
  final usuarioBloc = new UsuarioBloc();

  @override
  Widget build(BuildContext context) {
    usuarioBloc.obtenerUsuarios();
    return StreamBuilder<List<UsuarioModel>>(
      stream: usuarioBloc.scansStream,
      builder:
          (BuildContext context, AsyncSnapshot<List<UsuarioModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }

        final scans = snapshot.data;

        if (scans.length == 0) {
          return Center(
            child: Text('No hay información'),
          );
        }

        return ListView.builder(
            itemCount: scans.length,
            itemBuilder: (context, i) => Dismissible(
                key: UniqueKey(),
                background: Container(color: Colors.red),
                onDismissed: ( direction ) => usuarioBloc.borrarUsuario(scans[i].id),
                child: ListTile(
                  leading:
                      Icon(Icons.person_pin, color: Theme.of(context).primaryColor),
                  title: Text(
                      scans[i].nombre + ' ' +  scans[i].apellido + ' ' ),
                  subtitle: Text('CI: ${scans[i].id}' + '-Direccion:' + ' '+ scans[i].direccion),
                  trailing:
                      Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                  onTap: () {},
                  isThreeLine: true,
                )));
      },
    );
  }
}
