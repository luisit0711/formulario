import 'package:count/src/bloc/usuario_bloc.dart';
import 'package:count/src/models/UsuarioModel.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FormPageState();
  }
}

class _FormPageState extends State<FormPage> {
  int _id = 0;
  int currentIndex = 0;
  final _formKey = GlobalKey<FormState>();
  String _nombre, _apellido, _direccion;
  final usuarioBloc = new UsuarioBloc();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Center(
          child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Text(
              'Formulario',
              style: TextStyle(fontSize: 25.0),
              textAlign: TextAlign.center,
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Nro Carnet",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0))),
              keyboardType: TextInputType.number,
              onSaved: (input) => _id = int.parse(input),
            ),
            Text(
              '',
              style: TextStyle(fontSize: 10.0),
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Nombre",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0))),
              keyboardType: TextInputType.text,
              onSaved: (input) => _nombre = input,
            ),
            Text(
              '',
              style: TextStyle(fontSize: 10.0),
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Apellido",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0))),
              keyboardType: TextInputType.text,
              onSaved: (String input) {
                _apellido = input;
              },
            ),
            Text(
              '',
              style: TextStyle(fontSize: 10.0),
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Direccion",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0))),
              keyboardType: TextInputType.text,
              onSaved: (String input) {
                _direccion = input;
              },
            ),
          ],
          //mainAxisAlignment: MainAxisAlignment.start,
        ),
      )),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
          
            //guardar los datos en el modelo y en la base de datos
          }
          setState(() {
           UsuarioModel user = new UsuarioModel(
                id: _id,
                nombre: _nombre,
                apellido: _apellido,
                direccion: _direccion);
            usuarioBloc.agregarUsuario(user);
          });
        },
        child: Icon(Icons.add_box),
      ),
      //floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
    );
  }

  Widget _botones() {
    return Row(
      children: <Widget>[
        SizedBox(width: 30.0),
        FloatingActionButton(
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              UsuarioModel user = new UsuarioModel(
                  id: _id,
                  nombre: _nombre,
                  apellido: _apellido,
                  direccion: _direccion);
              usuarioBloc.agregarUsuario(user);
              //guardar los datos en el modelo y en la base de datos
            }
            setState(() {
              // print(_numero2);
              //contador = _numero1 + _numero2;
            });
          },
          child: Icon(Icons.add_box),
        ),
        Expanded(child: SizedBox())
      ],
    );
  }
}
