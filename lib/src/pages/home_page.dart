import 'package:count/src/bloc/usuario_bloc.dart';
import 'package:count/src/pages/contador_page.dart';
import 'package:count/src/pages/list_usuario.dart';
import 'package:flutter/material.dart';

import 'form_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //final usuarioBloc = new UsuarioBloc();
  int indexPagina = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Topicos'),
        actions: <Widget>[
          IconButton(
              icon: const Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
        centerTitle: true,
      ),
      body: _navegar(indexPagina),
      bottomNavigationBar: _crerNavegacion(),

    );
  }

  Widget _navegar(int pagina) {
    switch (pagina) {
      case 0:
        return FormPage();
      case 1:
        return ListUsuariosPage();
      case 2:
        return ContadorPage();

      default:
        return FormPage();
    }
  }

  Widget _crerNavegacion() {
    return BottomNavigationBar(
        currentIndex: indexPagina,
        onTap: (index) {
          setState(() {
            indexPagina = index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.person_add), title: Text('Nuevo')),
          BottomNavigationBarItem(icon: Icon(Icons.list), title: Text('Lista')),
          BottomNavigationBarItem(icon: Icon(Icons.note_add), title: Text('Lista'))
        ]);
  }
}
