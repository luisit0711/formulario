import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget{
@override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage>{
  int contador=0;
  final _formKey = GlobalKey<FormState>();
  int _numero1,_numero2;
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
     
    
      body: Center(
        child:Form(
          key: _formKey,  
        child: Column(
          children: <Widget>[
            Text('Suma',style: TextStyle(fontSize: 25.0),),
            Text('$contador', style: TextStyle(fontSize: 30.0)),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Numero 1",
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0)
                )
                
            ),
            keyboardType: TextInputType.number,
            onSaved: (input)=> _numero1= int.parse(input) ,
            
            ),
            Text('',style: TextStyle(fontSize: 25.0),),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Numero 2",
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0)
                )
            ),
            keyboardType: TextInputType.number,
            onSaved: (String input){
            _numero2= int.parse(input);
            print(_numero2);
            },
            ),
          ],
          
          mainAxisAlignment: MainAxisAlignment.center,

          ),
          
        )
      ),
      floatingActionButton: _botones(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Widget _botones(){
    return Row(
        children: <Widget>[
          SizedBox(width:30.0),
        FloatingActionButton(
        onPressed: (){
          if(_formKey.currentState.validate()){
            _formKey.currentState.save();
          }
          setState(() {
            print(_numero2);
            contador=_numero1+_numero2;
          });
        },
        child: Icon(Icons.add_box),

      ),
      Expanded(child: SizedBox())
      ],
      mainAxisAlignment: MainAxisAlignment.end,
      );
  }
}